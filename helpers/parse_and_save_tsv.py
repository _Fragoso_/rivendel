import pandas as pd

tsv_file = "cities_canada_usa.tsv"

csv_data_table = pd.read_table(tsv_file, sep="\t")
csv_data_table.to_csv("cities_canada_usa.csv", index=True)
