from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import City


class CitiesTests(APITestCase):
    fixtures = ["cities.json"]

    def setUp(self):
        self.url = reverse("city-list")

    def test_no_url_params(self):
        response = self.client.get(self.url, format="json")
        self.assertContains(
            response,
            "You haven't specify the string contained in the name",
            status_code=status.HTTP_400_BAD_REQUEST,
        )

    def test_wrong_url_params(self):
        wrong_param = {"name": "New"}
        response = self.client.get(self.url, wrong_param, format="json")
        self.assertContains(
            response,
            "You haven't specify the string contained in the name",
            status_code=status.HTTP_400_BAD_REQUEST,
        )

    def test_incomplete_measure_params(self):
        query_params = {"city_name": "Aberdeen", "longitude": 12.34}
        response = self.client.get(self.url, query_params, format="json")
        self.assertContains(
            response,
            "You need to enter both latitude and longitude",
            status_code=status.HTTP_400_BAD_REQUEST,
        )

    def test_obtain_suggestions_city_name_param(self):
        query_params = {"city_name": "Aberdeen"}
        response = self.client.get(self.url, query_params, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_obtain_suggestions_all_params(self):
        query_params = {
            "city_name": "Troy",
            "latitude": 34.32,
            "longitude": -90.87,
        }
        response = self.client.get(self.url, query_params, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
