import django_filters

from .models import City


class CityFilter(django_filters.FilterSet):
    city_name = django_filters.CharFilter(
        lookup_expr="startswith", field_name="name"
    )

    class Meta:
        model = City
        fields = ["city_name"]
