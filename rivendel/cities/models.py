from django.db import models
from django_countries.fields import CountryField


class City(models.Model):
    geoname_id = models.IntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    ascii_name = models.TextField(blank=True, null=True)
    alternate_names = models.TextField(blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    feature_class = models.TextField(blank=True, null=True)
    feature_code = models.TextField(blank=True, null=True)
    country_code = models.TextField(blank=True, null=True)
    cc2 = models.FloatField(blank=True, null=True)
    admin1_code = models.TextField(blank=True, null=True)
    admin2_code = models.FloatField(max_length=80, blank=True, null=True)
    admin3_code = models.FloatField(max_length=200, blank=True, null=True)
    admin4_code = models.FloatField(max_length=200, blank=True, null=True)
    population = models.BigIntegerField(blank=True, null=True)
    elevation = models.FloatField(blank=True, null=True)
    digital_elevation_model = models.IntegerField(blank=True, null=True)
    timezone = models.TextField(blank=True, null=True)
    modified_at = models.DateField(blank=True, null=True)

    class Meta:
        db_table = "cities"
