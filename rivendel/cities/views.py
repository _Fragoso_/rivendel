from django.shortcuts import render
from rest_framework import status
from rest_framework.mixins import ListModelMixin
from rest_framework.pagination import BasePagination
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from utils.operations import Compute

from .filters import CityFilter
from .models import City
from .serializers import CitySerializer


class CityViewSet(GenericViewSet, ListModelMixin):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    filterset_class = CityFilter

    def list(self, request, *args, **kwargs) -> object:
        if "city_name" not in request.GET.keys():
            return Response(
                {
                    "message: You haven't specify the string contained in the name"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        position_params = ["latitude", "longitude"]

        position_params_quantity = len(
            [True for param in request.GET.keys() if param in position_params]
        )

        if position_params_quantity == 1:
            return Response(
                {"message": "You need to enter both latitude and longitude"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)

        computed_cities = Compute.computed_cities(request.GET, serializer.data)

        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)

            computed_cities = Compute.computed_cities(
                request.GET, serializer.data
            )

            return self.get_paginated_response(
                {"suggestions": computed_cities}
            )

        return Response({"suggestions": computed_cities})
