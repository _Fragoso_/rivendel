from rest_framework.routers import SimpleRouter
from .views import CityViewSet


router = SimpleRouter()
router.register(r"suggestions", CityViewSet)
urlpatterns = router.urls
