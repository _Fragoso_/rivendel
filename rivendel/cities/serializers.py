from rest_framework import serializers
from rivendel.cities.models import City


class CitySerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = City
        fields = ["name", "latitude", "longitude"]

    def get_name(self, obj):
        return ", ".join([obj.name, obj.admin1_code, obj.timezone])
