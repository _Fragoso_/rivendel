# Generated by Django 3.0.3 on 2020-03-04 05:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("cities", "0003_auto_20200303_0532")]

    operations = [
        migrations.AlterField(
            model_name="city",
            name="admin2_code",
            field=models.FloatField(blank=True, max_length=80),
        ),
        migrations.AlterField(
            model_name="city",
            name="admin3_code",
            field=models.FloatField(blank=True, max_length=200),
        ),
        migrations.AlterField(
            model_name="city",
            name="admin4_code",
            field=models.FloatField(blank=True, max_length=200),
        ),
        migrations.AlterField(
            model_name="city", name="cc2", field=models.FloatField(blank=True)
        ),
        migrations.AlterField(
            model_name="city",
            name="country_code",
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AlterField(
            model_name="city", name="elevation", field=models.FloatField(blank=True)
        ),
    ]
