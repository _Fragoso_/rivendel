def sort_dict_list(dict_list: list, key: str) -> list:
    """
    Arguments:
        dict_list {list} -- List of dictionaries to sort.
        key {str} -- Key take as reference to sort the list.

    Returns:
        list -- Sorted dictionary list.
    """
    return sorted(
        dict_list, key=lambda list_element: list_element[key], reverse=True
    )
