from difflib import SequenceMatcher
from .handling_data import sort_dict_list


class Compute:
    """
    This class have methods to calculate the score
    of the filtered queryset.
    """

    def _compare_strings(self, first_string: str, second_string: str) -> float:
        return SequenceMatcher(None, first_string, second_string).ratio()

    def _get_longitude_latitude_percentage(
        self,
        serializer_data: int or float,
        query_data: int or float,
        percentage_contribution: int or float,
        lowest_error_percentage: float,
    ) -> float:
        percentage_error = abs(serializer_data - query_data) / query_data

        return (
            percentage_contribution * lowest_error_percentage
        ) / percentage_error

    def _get_percentage(
        self,
        serializer_data: int or float,
        query_data: int or float,
        percentage_contribution: int or float,
    ) -> float:
        return (serializer_data * percentage_contribution) / query_data

    def _get_percentage_contribution(self, total_items: int) -> float:
        FULL_COINCIDENCE = 1
        return FULL_COINCIDENCE / total_items

    def _add_score(cls, query_params: dict, cities_data: list) -> None:
        total_items = len(query_params)
        percentage_contribution = Compute()._get_percentage_contribution(
            total_items
        )

        FULL_STRING_MATCH = 1.0

        for city in cities_data:
            city_name = city["name"].split(", ")[0]

            strings_similitude = Compute()._compare_strings(
                query_params["city_name"], city_name
            )

            score = Compute()._get_percentage(
                strings_similitude, FULL_STRING_MATCH, percentage_contribution
            )

            if (
                "longitude" in query_params.keys()
                and "latitude" in query_params.keys()
            ):
                longitude = float(query_params.get("longitude"))
                latitude = float(query_params.get("latitude"))

                LOWEST_ERROR_PERCENTAGE = 1e-10

                score += Compute()._get_longitude_latitude_percentage(
                    city["longitude"],
                    longitude,
                    percentage_contribution,
                    LOWEST_ERROR_PERCENTAGE,
                )
                score += Compute()._get_longitude_latitude_percentage(
                    city["latitude"],
                    latitude,
                    percentage_contribution,
                    LOWEST_ERROR_PERCENTAGE,
                )

            city.update({"score": round(score, 1)})

    @classmethod
    def computed_cities(cls, query_params: dict, cities_data: list) -> dict:
        Compute()._add_score(query_params, cities_data)
        return sort_dict_list(cities_data, key="score")
